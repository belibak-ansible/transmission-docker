from django import forms
from django.forms import CharField, Form, PasswordInput

class LoginForm(forms.Form):
    login = forms.CharField(label='Login', max_length=100)
    passwd= forms.CharField(widget=PasswordInput(), label='Password', max_length=512)


