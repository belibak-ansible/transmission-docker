import os

from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.template.loader import get_template
from django.shortcuts import redirect

from .forms import LoginForm
import transdj.settings as settings

@login_required(login_url='/login/')
def gettars(request):
    t = get_template('tst.html')
    try:
        tars = os.listdir(settings.TRANSPATH)
    except FileNotFoundError:
        tars = os.listdir(settings.TRANSPATH_WIN)
    dct = {"tars" : tars}
    html = t.render(dct)
    return HttpResponse(html)

def removefile(filename):
    try:
        tars = os.listdir(settings.TRANSPATH)
    except FileNotFoundError:
        tars = os.listdir(settings.TRANSPATH_WIN)
    os.remove(f"{settings.TRANSPATH}/{filename}")

def rmtar(request, filename):
    print(filename)
    removefile(filename)
    return redirect("/")

def tlogin(request):
    dct = {}
    t = get_template('login.html')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['login']
            password = request.POST['passwd']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")

    else:
        form = LoginForm()
    dct = {"form": form}
    html = t.render(dct)
    return HttpResponse(html)
