FROM alpine:latest
RUN apk add --update --no-cache transmission-daemon tar
RUN adduser -h /home/voon -s /bin/sh -D -u 1000 voon
USER voon
RUN mkdir /home/voon/.transmission /home/voon/compressed 
ENTRYPOINT ["/usr/bin/transmission-daemon", "-f", "-g", "/home/voon/.transmission"] 
